/*  eslint-disable no-param-reassign */
import mutationsName from './mutationsName'
import initStore from './initStore'

export default {
  [mutationsName.SET_REFERER_URL]: (state, value) => {
    state.refererURL = value
  },

  [mutationsName.SET_TOKEN]: (state, value) => {
    state.token = value
  },

  [mutationsName.SET_RESET_MODEL]: (state) => {
    Object.assign(state, initStore)
  }
}
