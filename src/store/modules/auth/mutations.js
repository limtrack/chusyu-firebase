import mutationsName from './mutationsName'
import defaultStore from './defaultStore'

export default {
  [mutationsName.SET_AUTH]: (state, value) => {
    state = Object.assign({}, state, value)
  },

  [mutationsName.SET_RESET_AUTH]: (state) => {
    state = Object.assign(state, defaultStore)
  }
}
