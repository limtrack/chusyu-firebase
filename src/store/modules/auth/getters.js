export default {
  /**
   * return logged user status
   *
   * @param state
   * @returns {Boolean}
   */
  isAuthenticated: (state) => Boolean(state.id),

  /**
   * return logged user data
   *
   * @param state
   * @returns {Object}
   */
  getLoggedAuth: (state) => state || {}
}
