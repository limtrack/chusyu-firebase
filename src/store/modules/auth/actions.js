import mutationsName from './mutationsName'
import actionsName from './actionsName'

export default {
  [actionsName.UPDATE_AUTH]: ({ commit }, value) => {
    commit(mutationsName.SET_AUTH, value)
  },

  [actionsName.UPDATE_RESET_AUTH]: ({ commit }) => {
    commit(mutationsName.SET_RESET_AUTH)
  }
}
