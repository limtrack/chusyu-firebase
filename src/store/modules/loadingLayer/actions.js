import mutationsName from './mutationsName'
import actionsName from './actionsName'

export default {
  [actionsName.UPDATE_LOADING_LAYER_MODEL]: ({ commit }, value) => {
    commit(mutationsName.SET_LOADING_LAYER_MODEL, value)
  },

  [actionsName.UPDATE_LOADING_LAYER_RESET_MODEL]: ({ commit }) => {
    commit(mutationsName.SET_LOADING_LAYER_RESET_MODEL)
  }
}
