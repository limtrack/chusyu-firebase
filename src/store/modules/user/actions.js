import mutationsName from './mutationsName'
import actionsName from './actionsName'

export default {
  [actionsName.UPDATE_LOGGED_USER]: ({ commit }, value) => {
    commit(mutationsName.SET_LOGGED_USER, value)
  },

  [actionsName.UPDATE_USERS]: ({ commit }, value) => {
    commit(mutationsName.SET_USERS, value)
  },

  [actionsName.UPDATE_RESET_USER_MODEL]: ({ commit }) => {
    commit(mutationsName.SET_RESET_USER_MODEL)
  }
}
