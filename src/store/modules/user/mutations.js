import mutationsName from './mutationsName'
import defaultStore from './defaultStore'

export default {
  [mutationsName.SET_LOGGED_USER]: (state, value) => {
    state.loggedUser = Object.assign({}, state.loggedUser, value)
  },

  [mutationsName.SET_USERS]: (state, value) => {
    state.users = value
  },

  [mutationsName.SET_RESET_USER_MODEL]: (state) => {
    state = Object.assign(state, defaultStore)
  }
}
