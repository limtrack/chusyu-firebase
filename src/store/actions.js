import mutationsName from './mutationsName'
import actionsName from './actionsName'

export default {
  [actionsName.UPDATE_REFERER_URL]: ({ commit }, value) => {
    commit(mutationsName.SET_REFERER_URL, value)
  },

  [actionsName.UPDATE_TOKEN]: ({ commit }, value) => {
    commit(mutationsName.SET_TOKEN, value)
  },

  [actionsName.UPDATE_RESET_MODEL]: ({ commit }) => {
    commit(mutationsName.SET_RESET_MODEL)
  }
}
