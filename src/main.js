// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import firebase from 'firebase'
import App from './App'
import router from './router'
import store from './store'
import constants from './constants'
// Bootstrap
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import BootstrapVue from 'bootstrap-vue'
// Internationalization (I18n)
import VueI18n from 'vue-i18n'
import i18nMessages from './locale/messages'

// Firebase
firebase.initializeApp(constants.FIREBASE)

// Apply in Vue
Vue.use(BootstrapVue)
Vue.use(VueI18n)

const messages = i18nMessages
const i18n = new VueI18n({
  locale: constants.DEFAULT_LANGUAGE,
  messages
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  i18n,
  el: '#app',
  router,
  store,
  render: h => h(App),
}).$mount(`#${constants.APP_ID}`)
