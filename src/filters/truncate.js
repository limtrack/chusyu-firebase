import Vue from 'vue';

/**
 * Truncate string
 * Options availables
 *  - length -> size string to return, default 30
 *  - clamp -> put at the end string, default '...'
 *  - removeHtml -> remove HTML code, default false
 */
Vue.filter('truncate', (text = '', options) => {
  const {
    length,
    clamp,
    removeHtml,
  } = Object.assign({ length: 30, clamp: '...', removeHtml: false }, options);
  const currentText = removeHtml
    ? text.replace(/<[^>]*>/g, '')
    : text;

  if (currentText.length <= length) {
    return currentText;
  }

  let tcText = currentText.slice(0, length - clamp.length);
  let last = tcText.length - 1;

  while (last > 0 && tcText[last] !== ' ' && tcText[last] !== clamp[0]) {
    last -= 1;
  }

  // Fix for case when text dont have any `space`
  last = last || length - clamp.length;

  tcText = tcText.slice(0, last);

  return `${tcText}${clamp}`;
});
