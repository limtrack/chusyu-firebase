export default {

  // Setup Firebase
  FIREBASE: {
    apiKey: 'AIzaSyBXgubnPUi3ymFwpTMCQUe3VRyi-zajhWE',
    authDomain: 'chusyu-10df5.firebaseapp.com',
    databaseURL: 'https://chusyu-10df5.firebaseio.com',
    projectId: 'chusyu-10df5',
    storageBucket: 'chusyu-10df5.appspot.com',
    messagingSenderId: '422013853760',
    appId: '1:422013853760:web:98e53685960e505a8b3fa5'
  },

  // Vuex object is saved into the localstorage with this name
  KEY_VUEX_LOCALSTORAGE: 'chusyu',

  // Path where is saved the token into the localstorage
  PATH_TOKEN_LOCALSTORAGE: 'token.value',

  // Vuex modules
  VUEX_MODULES: {
    alert: 'alert',
    auth: 'auth',
    loadingLayer: 'loadingLayer',
    modal: 'modal',
    user: 'user'
  },

  // Flow app
  INIT_APP: 'ideas', // Initial path (view)
  LOGIN_PATH: 'user-login', // Login path

  // DOM elements
  APP_ID: 'app',
  APP_MAIN_ALERT_ID: 'app-main-alert',
  APP_MAIN_MODAL_ID: 'app-main-modal',
  APP_MAIN_LOADING_ID: 'app-main-loading-layer',

  // Language
  DEFAULT_LANGUAGE: 'es',
  AVAILABLE_LANGUAGES: ['en', 'es']
}
