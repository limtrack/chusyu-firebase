// Views
// import UsersSetup from '@/views/users/UsersSetup'
// import IdeasIndex from '@/views/ideas/IdeasIndex'
// import IdeasDetail from '@/views/ideas/IdeasDetail'
// import IdeasForm from '@/views/ideas/IdeasForm'
// import OffersIndex from '@/views/offers/OffersIndex'
// import OrdersIndex from '@/views/orders/OrdersIndex'
import UsersLogin from '@/views/users/UsersLogin'
// import UsersRegister from '@/views/users/UsersRegister'
// import UsersForget from '@/views/users/UsersForget'

export default [
  // {
  //   path: '/',
  //   component: IdeasIndex,
  //   meta: {
  //     isPublic: true
  //   }
  // },
  // {
  //   path: '/user',
  //   component: UsersSetup,
  //   meta: {
  //     sectionTitle: 'Setup'
  //   }
  // },
  // {
  //   path: '/user-register',
  //   component: UsersRegister,
  //   meta: {
  //     isPublic: true,
  //     sectionTitle: 'Register'
  //   }
  // },
  {
    path: '/user-login',
    component: UsersLogin,
    meta: {
      isPublic: true,
      sectionTitle: 'Login'
    }
  }
  // {
  //   path: '/user-forget',
  //   component: UsersForget,
  //   meta: {
  //     isPublic: true,
  //     sectionTitle: 'Recover'
  //   }
  // },
  // {
  //   path: '/ideas',
  //   component: IdeasIndex,
  //   meta: {
  //     hasSearch: true,
  //     isMainMenu: true,
  //     isPublic: true,
  //     sectionTitle: 'Ideas'
  //   }
  // },
  // {
  //   path: '/ideas/:nameSeo',
  //   component: IdeasDetail,
  //   meta: {
  //     isPublic: true,
  //     sectionTitle: 'Detail Idea'
  //   }
  // },
  // {
  //   path: '/ideas-form',
  //   component: IdeasForm
  // },
  // {
  //   label: 'Ofertas',
  //   path: '/offers',
  //   component: OffersIndex,
  //   meta: {
  //     hasSearch: true,
  //     isMainMenu: true,
  //     isPublic: true,
  //     sectionTitle: 'Offers'
  //   }
  // },
  // {
  //   label: 'Pedidos',
  //   path: '/orders',
  //   component: OrdersIndex,
  //   meta: {
  //     sectionTitle: 'Orders',
  //     isMainMenu: true
  //   }
  // }
]
