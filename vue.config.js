module.exports = {
  configureWebpack: {
    devtool: 'source-map'
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: '@import "@/styles/main.scss";'
      }
    }
  }
}